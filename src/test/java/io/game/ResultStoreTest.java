package io.game;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ResultStoreTest {
	private ResultStore resultStore;

	@BeforeEach
	void setUp() throws Exception {
		resultStore = new ResultStore();
	}

	@DisplayName("Constructor test ResultStore()")
	@Test
	void testResultStore() {
		assertNotNull(resultStore.getResultList(), "resultList should have been initiated");
	}

	@DisplayName("Constructor test ResultStore(Result result)")
	@Test
	void testResultStoreResult() {
		resultStore = new ResultStore(new Result(new Player(), new Player(), new Player()));
		assertNotNull(resultStore.getResultList(), "resultList should have been initiated");
		assertFalse(resultStore.getResultList().isEmpty(), "resultList should have a Result object.");
	}

	@DisplayName("Constructor test ResultStore(List<Result> resultList)")
	@Test
	void testResultStoreListOfResult() {
		resultStore = new ResultStore(new LinkedList<Result>());
		assertNotNull(resultStore.getResultList(), "resultList should have been initiated");
	}

	@Test
	void testGetResultList() {
		assertNotNull(resultStore.getResultList(), "resultList should have been initiated");
	}

	@Test
	void testAddResult() {
		resultStore = new ResultStore(new Result(new Player(), new Player(), new Player()));
		assertFalse(resultStore.getResultList().isEmpty(), "resultList should have a Result object.");
	}

	@Test
	void testRemoveAllResults() {
		Result result = new Result(new Player(), new Player(), new Player());
		resultStore = new ResultStore(result);
		resultStore.removeAllResults();
		assertNotNull(resultStore.getResultList(), "resultList should have been initiated");
		assertTrue(resultStore.getResultList().isEmpty(), "resultList should be empty");
	}

	@Test
	void testSetResultList() {
		Result result = new Result(new Player(), new Player(), new Player());
		Result result2 = new Result(new Player(), new Player(), new Player());
		ResultStore resultStore2 = new ResultStore();
		resultStore2.addResult(result);
		resultStore2.addResult(result2);
		LinkedList<Result> checkList = resultStore.getResultList();
		resultStore.setResultList(resultStore2.getResultList());

		// check if the old reference stays untouched.
		assertTrue(checkList.isEmpty(), "resultList should be empty");
		// check if a new list has been initiated and has the new objects included
		assertNotNull(resultStore.getResultList(), "resultList should have been initiated");
		assertFalse(resultStore.getResultList().isEmpty(), "resultList should have Result objects.");
	}

}
