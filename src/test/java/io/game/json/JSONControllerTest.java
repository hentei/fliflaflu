package io.game.json;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class JSONControllerTest {

	@Test
	public void testFiles() {
		assertNotNull(new JSONController("config.json"));
		assertNotNull(new JSONController("menu.json"));
		assertNotNull(new JSONController("strategies/paper.json"));
		assertNotNull(new JSONController("elements/shapes.json"));
	}

}
