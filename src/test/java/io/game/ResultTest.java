package io.game;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ResultTest {

	private Result result;

	@BeforeEach
	void setUp() throws Exception {
		Player p1 = new Player("p1");
		Player p2 = new Player("p2");
		p1.setPlayersChoice(3);
		p2.setPlayersChoice(1);
		result = new Result(p1, p1, p2);
	}

	@Test
	void testResultPlayerPlayerPlayerStrategyStrategy() {
		result = new Result(new Player(), new Player(), new Player(), new Strategy(), new Strategy());
	}

	@Test
	void testGetWinner() {
		assertEquals("p1", result.getWinner().getName(), "p1 should have won");
	}

	@Test
	void testGetP1() {
		assertEquals("p1", result.getP1().getName(), "expected was p1");
	}

	@Test
	void testGetP2() {
		assertEquals("p2", result.getP2().getName(), "expected was p2");
	}

	@Test
	void testGetStrategyP1() {
		assertNotNull(result.getStrategyP1(), "Strategy object of P1 should have more than null :)");
	}

	@Test
	void testGetStrategyP2() {
		assertNotNull(result.getStrategyP2(), "Strategy object of P2 should have more than null :)");
	}

	@Test
	public void testToString() {
		result.setStrategyP1(new Strategy(1, 1));
		result.setStrategyP2(new Strategy());
		assertEquals(result.toString(),
				"P1: p1(3), P2: p2(1), strategyP1: 1, strategyP2: -1, winner: p1");
	}
}
