package io.game;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConfigTest {

	Config config;

	@BeforeEach
	void setUp() throws Exception {
		config = new Config();
		config.setStrLanguage("de-de");
		config.setStrStrategy("paper");
		config.setStrAutopilot("off");
		assertNotNull(config, "config object was expected.");
	}

	@Test
	public void testSetStrLanguageConfig() {
		config.setStrLanguage("en-en");
		assertEquals(config.getStrLanguage(), "en-en");
	}

	public void testSetStrStrategyConfig() {
		config.setStrStrategy("rock");
		assertEquals(config.getStrLanguage(), "rock");
	}

	public void testSetStrAutopilotConfig() {
		config.setStrAutopilot("on");
		assertEquals(config.getStrLanguage(), "on");
	}

	@Test
	public void testToString() {
		assertEquals(config.toString(), "language: de-de, strategy: paper, autopilot: off");
	}

	@Test
	public void testGetStrLanguage() {
		assertEquals(config.getStrLanguage(), "de-de");
	}

	@Test
	public void testGetStrStrategy() {
		assertEquals(config.getStrStrategy(), "paper");
	}

	@Test
	public void testGetStrAutopilot() {
		assertEquals(config.getStrAutopilot(), "off");
	}

}
