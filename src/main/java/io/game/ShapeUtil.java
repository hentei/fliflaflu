package io.game;

public class ShapeUtil {

	private AssetHandler assetHandler;

	public ShapeUtil(AssetHandler assetHandler) {
		this.assetHandler = assetHandler;
	}

	public Shape getShapById(int shapeId) {
		return assetHandler.getShapes().get(shapeId);
	}

}
