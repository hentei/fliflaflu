package io.game;

import java.util.List;

public class DecisionMaker {

	private AssetHandler assetHandler;

	public DecisionMaker(AssetHandler assetHandler) {
		this.assetHandler = assetHandler;
	}

	public Player getWinner(Player player1, Player player2) {
		List<String> player1HitList = assetHandler.getShapes().get(player1.getPlayersChoice() - 1).getHitList();
		List<String> player2HitList = assetHandler.getShapes().get(player2.getPlayersChoice() - 1).getHitList();
		String player1Choice = assetHandler.getShapes().get(player1.getPlayersChoice() - 1).getType();
		String player2Choice = assetHandler.getShapes().get(player2.getPlayersChoice() - 1).getType();
		if (hasHit(player1HitList, player2Choice))
			return player1;
		if (hasHit(player2HitList, player1Choice))
			return player2;
		Player draw = new Player(player1.getName() + " & " + player2.getName());
		draw.setPlayersChoice(player1.getPlayersChoice());
		return draw;
	}

	private boolean hasHit(List<String> player1HitList, String player2Choice) {
		boolean hit = false;
		for (String str : player1HitList) {
			if (str.equals(player2Choice))
				hit = true;
		}
		return hit;
	}

}
