package io.game;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Menu {

	@SerializedName("title")
	private String strTitle;
	@SerializedName("continu_msg")
	private String strContinueMsg;
	@SerializedName("choose_msg")
	private String strChooseMsg;
	@SerializedName("pick_msg")
	private String strPickMsg;
	@SerializedName("separtor_line")
	private String strSeparotorLine;
	@SerializedName("won_msg")
	private String strWonMsg;
	@SerializedName("draw_msg")
	private String strDrawMsg;
	@SerializedName("input_error_msg")
	private String strInputErrorMsg;
	@SerializedName("player_name")
	private String strPlayerName;
	@SerializedName("computer_name")
	private String strComputerName;
	@SerializedName("exit")
	private String strExit;	
	@SerializedName("bye_msg")
	private String strByeMsg;	
	@SerializedName("lets_start_msgList")
	private List<String> letsStartMsgList;
	
	public String toString() {
		return String.format("title: %s; "
				+ "continu_msg: %s; "
				+ "choose_msg: %s; "
				+ "pick_msg: %s; "
				+ "separtor_line: %s; "
				+ "won_msg: %s; "
				+ "draw_msg: %s; "
				+ "input_error_msg: %s; "
				+ "player_name: %s; "
				+ "computer_name: %s; "
				+ "exit: %s; "
				+ "bye_msg: %s; "
				+ "lets_start_msgList: %s", 
				getStrTitle(),
				getStrContinueMsg(),
				getStrChooseMsg(),
				getStrPickMsg(),
				getStrSeparotorLine(),
				getStrWonMsg(),
				getStrDrawMsg(),
				getStrPlayerName(),
				getStrComputerName(),
				getStrExit(),
				getStrByeMsg(),
				getStrInputErrorMsg(),
				getLetsStartMsgList().toString());
	}

	public String getStrTitle() {
		return strTitle;
	}

	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}

	public String getStrContinueMsg() {
		return strContinueMsg;
	}

	public void setStrContinueMsg(String strContinueMsg) {
		this.strContinueMsg = strContinueMsg;
	}

	public String getStrChooseMsg() {
		return strChooseMsg;
	}

	public void setStrChooseMsg(String strChooseMsg) {
		this.strChooseMsg = strChooseMsg;
	}

	public String getStrPickMsg() {
		return strPickMsg;
	}

	public void setStrPickMsg(String strPickMsg) {
		this.strPickMsg = strPickMsg;
	}

	public String getStrSeparotorLine() {
		return strSeparotorLine;
	}

	public void setStrSeparotorLine(String strSeparotorLine) {
		this.strSeparotorLine = strSeparotorLine;
	}

	public String getStrWonMsg() {
		return strWonMsg;
	}

	public void setStrWonMsg(String strWonMsg) {
		this.strWonMsg = strWonMsg;
	}

	public String getStrDrawMsg() {
		return strDrawMsg;
	}

	public void setStrDrawMsg(String strDrawMsg) {
		this.strDrawMsg = strDrawMsg;
	}

	public String getStrInputErrorMsg() {
		return strInputErrorMsg;
	}

	public void setStrInputErrorMsg(String strInputErrorMsg) {
		this.strInputErrorMsg = strInputErrorMsg;
	}

	public String getStrPlayerName() {
		return strPlayerName;
	}

	public void setStrPlayerName(String strPlayerName) {
		this.strPlayerName = strPlayerName;
	}

	public String getStrComputerName() {
		return strComputerName;
	}

	public void setStrComputerName(String strComputerName) {
		this.strComputerName = strComputerName;
	}
	
	public String getStrExit() {
		return strExit;
	}

	public void setStrExit(String strExit) {
		this.strExit = strExit;
	}	
	
	public String getStrByeMsg() {
		return strByeMsg;
	}

	public void setStrByeMsg(String strByeMsg) {
		this.strByeMsg = strByeMsg;
	}		

	public List<String> getLetsStartMsgList() {
		return letsStartMsgList;
	}

	public void setLetsStartMsgList(List<String> letsStartMsgList) {
		this.letsStartMsgList = letsStartMsgList;
	}
	
	
}
