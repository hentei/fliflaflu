package io.game;

import com.google.gson.Gson;

import io.game.json.JSONController;

public class ConfigUtil {
	private static final String CONFIG_FILE = "config.json";
	private Config config;
	JSONController jsonController;
	
	public ConfigUtil() {
		initComponents();
	}

	private void initComponents() {
		config = new Config();
		jsonController = new JSONController(CONFIG_FILE);
		readConfig();
	}

	private void readConfig() {
		Gson gson = new Gson();
		config = gson.fromJson(jsonController.getJsonReader(), Config.class);
	}
	
	public Config getConfig() {
		return this.config;
	}
}
