package io.game.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class JSONController {
	private String path;
	ClassLoader classLoader;


	public JSONController(String path) {
		this.path = path;
		classLoader = getClass().getClassLoader();
	}

	private JsonReader readFromFile() {	
		try {
			JsonReader reader = new JsonReader(new FileReader(new File(classLoader.getResource(path).getFile())));
			return reader;

		} catch (FileNotFoundException e) {
			System.out.println(path);
			e.printStackTrace();
		}

		return null;
	}

	public JsonReader getJsonReader() { 
		return readFromFile();
	}

	public String getJsonAsString() {
		Gson gson = new Gson();
		List<Object> objList = gson.fromJson(getJsonReader(), Object.class);
		return objList.toString();
	}

}
