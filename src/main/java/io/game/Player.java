package io.game;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private String name;
	private int playersChoice;

	public Player() {
		this("player");
	}

	public Player(String name) {
		this(name, 0);
	}

	public Player(Player player) {
		this(player.getName(), player.getPlayersChoice());
	}

	public Player(String name, int playersChoice) {
		setName(name);
		setPlayersChoice(playersChoice);
	}

	public int getPlayersChoice() {
		return playersChoice;
	}

	public void setPlayersChoice(int playersChoice) {
		this.playersChoice = playersChoice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
