package io.game;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import io.game.json.JSONController;

public class AssetHandler {

	private static final String SHAPES_FILE = "elements/shapes.json";
	private List<Shape> shapeList;
	JSONController jsonController;
	private Config config;

	public AssetHandler(Config config) {
		this.config = config;
		initComponents();
	}

	private void initComponents() {
		shapeList = new ArrayList<Shape>();
		jsonController = new JSONController(SHAPES_FILE);
		generateShapeList();
	}

	public List<Shape> getShapes() {
		return this.shapeList;
	}

	private void generateShapeList() {
		for (Shape shape : getShapeListFromJson()) {
			addShapeToList(shape);
		}
	}

	private List<Shape> getShapeListFromJson() {
		JsonReader jsonReader = jsonController.getJsonReader();
		Gson gson = new Gson();
		JsonArray jArr = gson.fromJson(jsonReader, JsonArray.class);
		JsonObject jObj = new JsonObject();
		for (JsonElement e : jArr) {
			jObj = e.getAsJsonObject();
			if (jObj.get(config.getStrLanguage()) != null)
				break;
		}

		Type type = new TypeToken<List<Shape>>() {
		}.getType();
		List<Shape> shapeList = gson.fromJson(jObj.get(config.getStrLanguage()), type);
		return shapeList;
	}

	private void addShapeToList(Shape shape) {
		if (!this.shapeList.contains(shape))
			shapeList.add(shape);
	}

}
