package io.game;

import java.util.List;

public interface Strategyset {
	List<Strategy> getStrategyList();
}
