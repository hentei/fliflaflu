package io.game;

import java.util.ArrayList;
import java.util.List;

public class Shape {
	private String type;
	private List<String> hitList;
	
	public Shape() {
		this.type = "";
		this.hitList = new ArrayList<String>();
	}
	
	public Shape(Shape shape) {
		setType(shape.getType());
		setHitList(shape.getHitList());
	}

	public String toString() {
		return String.format("type: %s - hitList: %s", type, hitList.toString());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getHitList() {
		return hitList;
	}

	public void setHitList(List<String> hitList) {
		this.hitList = hitList;
	}
}
