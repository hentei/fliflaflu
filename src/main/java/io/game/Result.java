package io.game;

public class Result {

	private Player winner;
	private Player p1;
	private Player p2;
	private Strategy strategyP1;
	private Strategy strategyP2;

	public Result(Player winner, Player p1, Player p2) {
		this(winner, p1, p2, new Strategy());
	}

	public Result(Player winner, Player p1, Player p2, Strategy strategyP1) {
		this(winner, p1, p2, strategyP1, new Strategy());
	}

	public Result(Player winner, Player p1, Player p2, Strategy strategyP1, Strategy strategyP2) {
		setP1(p1);
		setP2(p2);
		setStrategyP1(strategyP1);
		setStrategyP2(strategyP2);
		setWinner(winner);
	}

	public Player getWinner() {
		return winner;
	}

	public void setWinner(Player winner) {
		this.winner = winner;
	}

	public Player getP1() {
		return p1;
	}

	public void setP1(Player p1) {
		this.p1 = new Player(p1);
	}

	public Player getP2() {
		return p2;
	}

	public void setP2(Player p2) {
		this.p2 = new Player(p2);
	}

	public Strategy getStrategyP1() {
		return strategyP1;
	}

	public void setStrategyP1(Strategy strategyP1) {
		this.strategyP1 = strategyP1;
	}

	public Strategy getStrategyP2() {
		return strategyP2;
	}

	public void setStrategyP2(Strategy strategyP2) {
		this.strategyP2 = strategyP2;
	}

	@Override
	public String toString() {
		return String.format("P1: %s(%s), P2: %s(%s), strategyP1: %d, strategyP2: %d, winner: %s",
				getP1().getName(), getP1().getPlayersChoice(), getP2().getName(), getP2().getPlayersChoice(),
				getStrategyP1().getShapeId(), getStrategyP2().getShapeId(), getWinner().getName());
	}
}
