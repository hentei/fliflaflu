package io.game;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.game.json.JSONController;

public class StrategyUtil implements Strategyset {
	private static final String STRATEGIES_PATH = "strategies/";
	private String filename;
	private List<Strategy> strategyList;
	JSONController jsonController;

	public StrategyUtil(Config config) {
		this.filename = config.getStrStrategy() + ".json";
		System.out.println(STRATEGIES_PATH + this.filename);
		initComponents();
	}

	private void initComponents() {
		strategyList = new ArrayList<Strategy>();
		jsonController = new JSONController(STRATEGIES_PATH + filename);
		readStrategy();
	}

	private void readStrategy() {
		Gson gson = new Gson();
		Type type = new TypeToken<List<Strategy>>() {
		}.getType();
		strategyList = gson.fromJson(jsonController.getJsonReader(), type);
		// System.out.println(strategyList.toString());
	}

	@Override
	public List<Strategy> getStrategyList() {
		return this.strategyList;
	}
}
