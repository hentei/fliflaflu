package io.game;

public class GameSession {
	Playground playground;

	public GameSession(Player[] player) {
		playground = new Playground(player);
	}

	public void start() {
		playground.start();
	}
	
	public void stop() {
		playground.stop();
	}

	public void kill() {
		stop();
		playground = null;		
	}
}
