package io.game;

import java.util.LinkedList;

public class ResultStore {

	private LinkedList<Result> resultList;

	public ResultStore() {
		initComponents();
	}

	public ResultStore(Result result) {
		initComponents();
		addResult(result);
	}

	public ResultStore(LinkedList<Result> resultList) {
		initComponents();
	}

	private void initComponents() {
		this.resultList = new LinkedList<Result>();
	}

	public LinkedList<Result> getResultList() {
		return new LinkedList<Result>(this.resultList);
	}

	public void addResult(Result result) {
		if (!this.resultList.contains(result))
			this.resultList.add(result);
	}

	public void removeAllResults() {
		this.resultList.clear();

	}

	public void setResultList(LinkedList<Result> resultList) {
		if (!resultList.isEmpty())
			this.resultList = new LinkedList<Result>(resultList);
	}
}
