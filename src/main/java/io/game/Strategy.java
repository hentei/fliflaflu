package io.game;

import com.google.gson.annotations.SerializedName;

public class Strategy {

	@SerializedName("shape_id")
	private int shapeId;
	private int laps;

	public Strategy() {
		this(-1, 1);
	}

	public Strategy(int shapeId, int laps) {
		setShapeId(shapeId);
		setLaps(laps);
	}

	public String toString() {
		return String.format("shape_id: %d - laps: %d", shapeId, laps);
	}

	public int getShapeId() {
		return shapeId;
	}

	public void setShapeId(int shapeId) {
		if (laps < 0)
			this.shapeId = -1;
		else
			this.shapeId = shapeId;
	}

	public int getLaps() {
		return laps;
	}

	public void setLaps(int laps) {
		if (laps < 1)
			this.laps = 1;
		else
			this.laps = laps;
	}
}
