package io.game;

import java.util.LinkedList;

public class Server {

	private LinkedList<GameSession> gsList;
	private Player p1;
	private Player p2;
	private Menu menu;

	public static void main(String[] args) {
		Server server = new Server();
		server.initComponents();
		server.startNewGameSession();
	}

	private void initComponents() {
		gsList = new LinkedList<GameSession>();
		menu = new MenuUtil(new ConfigUtil().getConfig()).getMenu();
		p1 = new Player(menu.getStrPlayerName());
		p2 = new Player(menu.getStrComputerName());
	}

	public void startNewGameSession() {
		Player[] player = { p1, p2 };
		GameSession gs = new GameSession(player);
		addGameSession(gs);
		gs.start();
	}

	public void addGameSession(GameSession gameSession) {
		if (!gsList.contains(gameSession))
			this.gsList.add(gameSession);
	}

	public void removeGameSession(GameSession gameSession) {
		if (gsList.contains(gameSession))
			this.gsList.remove(gameSession);
	}
}
