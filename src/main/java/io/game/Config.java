package io.game;

import com.google.gson.annotations.SerializedName;

public class Config {
	@SerializedName("language")
	private String strLanguage;
	@SerializedName("strategy")
	private String strStrategy;
	@SerializedName("autopilot")
	private String strAutopilot;

	public String toString() {
		return String.format("language: %s, strategy: %s, autopilot: %s", 
				getStrLanguage(), getStrStrategy(),	getStrAutopilot());
	}

	public String getStrLanguage() {
		return strLanguage;
	}

	public void setStrLanguage(String strLanguage) {
		this.strLanguage = strLanguage;
	}

	public String getStrStrategy() {
		return strStrategy;
	}

	public void setStrStrategy(String strStrategy) {
		this.strStrategy = strStrategy;
	}

	public String getStrAutopilot() {
		return strAutopilot;
	}

	public void setStrAutopilot(String strAutopilot) {
		this.strAutopilot = strAutopilot;
	}
}
