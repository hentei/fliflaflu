package io.game;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import io.game.json.JSONController;

public class MenuUtil {
	private static final String MENU_FILE = "menu.json";
	private Menu menu;
	private Config config;
	JSONController jsonController;
	
	public MenuUtil(Config config) {
		this.config = config;
		initComponents();
	}

	private void initComponents() {
		menu = new Menu();
		jsonController = new JSONController(MENU_FILE);
		readMenu();
	}

	private void readMenu() {
		JsonReader jsonReader = jsonController.getJsonReader();
		Gson gson = new Gson();
		JsonArray jArr = gson.fromJson(jsonReader, JsonArray.class);
		JsonObject jObj = new JsonObject();
		for (JsonElement e : jArr) {
			jObj = e.getAsJsonObject();
			if (jObj.get(config.getStrLanguage()) != null)
				break;
		}
		
		menu = gson.fromJson(jObj.get(config.getStrLanguage()), Menu.class);
	}
	
	public Menu getMenu() {
		return this.menu;
	}
}
