package io.game;

import java.io.Console;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Playground {

	private Config config;
	private Menu menu;
	private AssetHandler ah;
	private Player player1;
	private Player player2;
	private Random rndGen;
	private Console con;
	private DecisionMaker dm;
	Output output;
	private GameMode gm;
	private ResultStore resultStore;
	private Strategy defaultStrategyP1;
	private Strategy defaultStrategyP2;
	char cont;

	public Playground(Player[] player) {
		initComponents();
		this.player1 = player[0];
		this.player2 = player[1];
	}

	public void start() {
		gameLoop();
	}

	private void initComponents() {
		config = new ConfigUtil().getConfig();
		menu = new MenuUtil(config).getMenu();
		ah = new AssetHandler(config);
		rndGen = new Random();
		con = System.console();
		dm = new DecisionMaker(ah);
		output = Output.SYSO;
		gm = new GameMode(config);
		resultStore = new ResultStore();
		defaultStrategyP1 = new Strategy(-1, 1);
		defaultStrategyP2 = new Strategy(0, 1);
		cont = 'y';
	}

	private void output(String str) {
		switch (output) {
		case CONSOLE:
			con.printf("%s\n", str);
			break;
		case SYSO:
			System.out.println(str);
			break;
		}
	}

	private void gameLoop() {

		Scanner input = new Scanner(System.in);
		while (cont == 'y' || cont == 'Y') {
			if (gm.isAutopilotEnabled()) {
				resultStore.removeAllResults();
				startBotPlay();
				if (player1.getPlayersChoice() != 0) {
					output(menu.getStrContinueMsg());
					cont = input.next().charAt(0);
				} else {
					cont = 'n';
				}
			} else {
				nextRound(defaultStrategyP1, defaultStrategyP2);
				if (player1.getPlayersChoice() != 0) {
					output(menu.getStrContinueMsg());
					cont = input.next().charAt(0);
				} else {
					cont = 'n';
				}
			}
		}
		output(menu.getStrByeMsg());
		input.close();
	}

	private void displayBotResult() {
		int winCountP1 = 0;
		int winCountP2 = 0;
		int winCountBoth = 0;
		Player p1;
		Player p2;
		output(menu.getStrSeparotorLine());
		for (Result s : resultStore.getResultList()) {
			if (s.getWinner().getName().equals(player1.getName()))
				winCountP1++;
			else if (s.getWinner().getName().equals(player2.getName()))
				winCountP2++;
			else
				winCountBoth++;
			p1 = s.getP1();
			p2 = s.getP2();
			System.out.format("%s[§%s]: %10s | %-10s %10s[§%s]\n", p1.getName(),
					convertToShape(s.getStrategyP1().getShapeId()), convertToShape(p1.getPlayersChoice()),
					convertToShape(p2.getPlayersChoice()), ":" + p2.getName(),
					convertToShape(s.getStrategyP2().getShapeId()));
		}
		output(menu.getStrSeparotorLine());
		output(player1.getName() + " (-:\t\t\t" + winCountP1 + "\n" + player2.getName() + " (o:\t\t\t" + winCountP2
				+ "\n" + player1.getName() + " & " + player2.getName() + " <-:\t\t" + winCountBoth);
		output(menu.getStrSeparotorLine());

	}

	private String convertToShape(int id) {
		String str = "-";
		if (id > 0)
			str = ah.getShapes().get(id - 1).getType();
		if (id == 0)
			str = "*";
		return str;
	}

	private void nextRound(Strategy strategyP1, Strategy strategyP2) {
		if (!gm.isAutopilotEnabled()) {
			showPlayOptions();
			askPlayerToChoose();
		}
		if (player1.getPlayersChoice() != 0) {
			rollForPlayer(player2);
			if (!gm.isAutopilotEnabled())
				displayResult(dm.getWinner(player1, player2));
			else
				collectResult(dm.getWinner(player1, player2), strategyP1, strategyP2);
		}
	}

	// collect the result and put it in the resultstore
	private void collectResult(Player winner, Strategy strategyP1, Strategy strategyP2) {
		Result result = new Result(winner, player1, player2, strategyP1, strategyP2);
		resultStore.addResult(result);

	}

	private void startBotPlay() {
		for (Strategy s : gm.getStrategyList()) {
			for (int i = 0; i < s.getLaps(); i++) {
				if (s.getShapeId() == 0)
					rollForPlayer(player1);
				else
					player1.setPlayersChoice(s.getShapeId());
				nextRound(s, defaultStrategyP2);
			}
			displayBotResult();
			resultStore.removeAllResults();
		}
	}

	private void rollForPlayer(Player player) {
		int range = ah.getShapes().size();
		int result = rndGen.nextInt(range) + 1;
		player.setPlayersChoice(result);
	}

	private void displayResult(Player winner) {
		output("");
		output(getRandomGameTitle());
		output(menu.getStrSeparotorLine());
		output(player1.getName() + " " + menu.getStrPickMsg() + ": "
				+ ah.getShapes().get(player1.getPlayersChoice() - 1).getType());
		output(player2.getName() + " " + menu.getStrPickMsg() + ": "
				+ ah.getShapes().get(player2.getPlayersChoice() - 1).getType());

		System.out.println(menu.getStrSeparotorLine());
		if (winner != null)
			output(winner.getName() + " " + menu.getStrWonMsg());
		else
			output(menu.getStrDrawMsg());
	}

	private void askPlayerToChoose() {
		Scanner scanner = new Scanner(System.in);

		int number;
		do {
			output(menu.getStrChooseMsg());
			while (!scanner.hasNextInt()) {
				String input = scanner.next();
				output("\"" + input + "\" " + menu.getStrInputErrorMsg() + "\n");
				output(menu.getStrChooseMsg());
			}
			number = scanner.nextInt();
		} while (number < 0 || number > ah.getShapes().size());
		player1.setPlayersChoice(number);
		if (number == 0)
			scanner.close();
	}

	private void showPlayOptions() {
		List<Shape> listShape = ah.getShapes();

		output(menu.getStrTitle());
		int c = 1;
		for (int i = 0; i < listShape.size(); i++) {
			if (!listShape.isEmpty())
				output("(" + c++ + ") " + listShape.get(i).getType());
		}
		output(menu.getStrSeparotorLine());
		output("(0) " + menu.getStrExit());
	}

	private String getRandomGameTitle() {
		return menu.getLetsStartMsgList().get(rndGen.nextInt(menu.getLetsStartMsgList().size()));
	}

	public void stop() {
		cont = 'q';
	}
}
