package io.game;

import java.util.ArrayList;
import java.util.List;

public class GameMode implements Strategyset {
	private List<Strategy> strategyList;
	private Config config;
	private boolean autopilot;

	public GameMode(Config config) {
		this.config = config;
		initCompunents();
	}

	private void initCompunents() {
		strategyList = new ArrayList<Strategy>();
		if (config.getStrStrategy().isEmpty() || config.getStrAutopilot().contains("off"))
			setAutopilot(false);
		else
			setAutopilot(true);
		chooseGameMode();
	}

	private void chooseGameMode() {
		if (isAutopilotEnabled())
			createStrategy();
	}

	private void createStrategy() {
		setStrategyList(new StrategyUtil(config).getStrategyList());
	}

	@Override
	public List<Strategy> getStrategyList() {
		return strategyList;
	}

	private void setStrategyList(List<Strategy> strategyList) {
		this.strategyList = strategyList;
	}

	public boolean isAutopilotEnabled() {
		return autopilot;
	}

	public void setAutopilot(boolean bool) {
		this.autopilot = bool;
	}
}
